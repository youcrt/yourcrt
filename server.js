#!/usr/bin/env node

var express = require('express');
var app = express();
var port = 7676;
var server = app.listen(port, function () {
  console.log('YourCRT app listening on port ' + port)
})

app.use(express.static('./public'));
